#![feature(lang_items)]
#![windows_subsystem = "console"]
use core::ffi::{c_void, c_int, c_char, c_double};

#[link(name="kernel32")]
extern "system" {
    pub fn CreateMutexA(_: *mut c_void, _: c_int, _: *mut c_char);
    pub fn Sleep(_: c_double);
}

fn main() {
    unsafe {
        CreateMutexA(0 as *mut _, 1, "ROBLOX_singletonMutex".as_ptr() as *mut _);
        Sleep(-1.0);
    }
}
